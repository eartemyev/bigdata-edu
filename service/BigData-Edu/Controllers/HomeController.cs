﻿using System.Web.Hosting;
using System.Web.Mvc;
using BigData_Edu.Repositories;

namespace BigData_Edu.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return FactsYes();
        }
        public ActionResult Details(long n1, long n2)
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                var model = repo.PairData(n1, n2);
                return View(model);
            }
        }
        public ActionResult Map(long n1, long n2)
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                var model = repo.PairData(n1, n2);
                return View(model);
            }
        }


        public ActionResult FactsYes()
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                ViewBag.LeftMenu = "1";
                ViewBag.Title = "Факты ДА";
                var model = repo.FactsYes();
                return View("Index", model);
            }
        }
        public ActionResult FactsNo()
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                ViewBag.LeftMenu = "2";
                ViewBag.Title = "Факты НЕТ";
                var model = repo.FactsNo();
                return View("Index", model);
            }
        }
        public ActionResult FindPairs()
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                ViewBag.LeftMenu = "3";
                ViewBag.Title = "Поиск Пар";
                var model = repo.FindPairs();
                return View("FindPairs", model);
            }
        }


        public ActionResult Test()
        {
            return View();
        }
    }
}