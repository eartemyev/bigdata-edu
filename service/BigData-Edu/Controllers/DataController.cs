﻿using System.Web.Hosting;
using System.Web.Http;
using BigData_Edu.Models;
using BigData_Edu.Repositories;

namespace BigData_Edu.Controllers
{
    public class DataController : ApiController
    {
        // get: /pairs
        [Route("pairs")]
        public Pairs GetPairs()
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                return repo.Pairs();
            }
        }

        // get: /pair-data?n1=158528850493&n2=158524011021
        [Route("pair-data")]
        public Pair GetPairData(long n1, long n2)
        {
            using (var repo = new Repository(HostingEnvironment.MapPath("~/")))
            {
                return repo.PairData(n1, n2);
            }
        }
    }
}