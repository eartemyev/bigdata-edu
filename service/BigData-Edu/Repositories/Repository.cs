﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using BigData_Edu.Models;

namespace BigData_Edu.Repositories
{
    public class Repository : IDisposable
    {
        private readonly string _rootPath;

        public Repository(string rootPath)
        {
            _rootPath = rootPath;
        }


        public Pairs Pairs()
        {
            var model = LoadModel<Pairs>(@"Data\Pairs.xml");
            //var xml = SaveModel(new Pairs().MockData());
            return model;
        }

        public Pair PairData(long n1, long n2)
        {
            /*var model1 = LoadModel<PairData>(@"Data\FactsYes.xml");
            var model2 = LoadModel<PairData>(@"Data\FactsNo.xml");*/
            var model = LoadModel<PairData>(@"Data\PairData.xml");
            var list = model.Result;
            return list.FirstOrDefault(e => e.N1 == n1 && e.N2 == n2);
        }
        //public PairData PairDataAll()
        //{
        //    var model = LoadModel<PairData>(@"Data\PairData.xml");
        //    return model;
        //}


        public PairData FactsYes()
        {
            var model = LoadModel<PairData>(@"Data\FactsYes.xml");
            return model;
        }
        public Pair FactsYes(long n1, long n2)
        {
            var model = FactsYes();
            return model.Result.FirstOrDefault(e => e.N1 == n1 && e.N2 == n2);
        }

        public PairData FactsNo()
        {
            var model = LoadModel<PairData>(@"Data\FactsNo.xml");
            return model;
        }
        public Pair FactsNo(long n1, long n2)
        {
            var model = FactsNo();
            return model.Result.FirstOrDefault(e => e.N1 == n1 && e.N2 == n2);
        }

        public FindPairs FindPairs()
        {
            var model = LoadModel<FindPairs>(@"Data\FindPairs.xml");
            return model;
        }


        public void Dispose() {}

        #region Utils
        private T LoadModel<T>(string path)
        {
            var serializer =  new XmlSerializer(typeof(T));
            using (var stream = File.OpenRead(Path.Combine(_rootPath, path)))
            {
                return (T)serializer.Deserialize(stream);
            }
        }
        private string SaveModel<T>(T model)
        {
            var serializer =  new XmlSerializer(typeof(T));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, model);
                return writer.ToString();
            }
        }
        #endregion
    }
}