﻿var eartemyev = {};
eartemyev.bigData = {};

eartemyev.bigData.selectRow = function (_this) {
    $(".fact-list tr").removeClass("fact-list-active");
    $(_this).addClass("fact-list-active");
}
eartemyev.bigData.showDetails = function (url, n1, n2) {
    $('#details').html("загрузка...");
    $('#details').load(url, { n1: n1, n2: n2 }, function (response, status, xhr) {
        if (status === "error") {
            var msg = "Sorry but there was an error: ";
            $("#details").html(msg + xhr.status + " " + xhr.statusText);
        }
    });
}
eartemyev.bigData.showMap = function (url, n1, n2) {
    $('#map').html("загрузка...");
    $('#map').load(url, { n1: n1, n2: n2 }, function (response, status, xhr) {
        if (status === "error") {
            var msg = "Sorry but there was an error: ";
            $("#map").html(msg + xhr.status + " " + xhr.statusText);
        }
    });
}
