﻿using System.Collections.Generic;

namespace BigData_Edu.Models
{
    public class FindPairs
    {
        public List<FindPair> Result { get; set; }
    }

    public class FindPair
    {
        public long N1 { get; set; }
        public List<FindPairItem> Items;
    }

    public class FindPairItem
    {
        public long N2 { get; set;}
        public decimal M { get; set;}
        public bool HasFact { get; set;}
    }
}