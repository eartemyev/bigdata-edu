﻿using System.Collections.Generic;

namespace BigData_Edu.Models
{
    public class Pairs
    {
        public List<Pair> Result { get; set; }
    }

    public class Pair
    {
        public int RowNumber { get; set; }
        public long N1 { get; set; }
        public long N2 { get; set; }
        public double Metrica { get; set; }

        public Points Points { get; set; }
    }
}