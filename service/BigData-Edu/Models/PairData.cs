﻿using System.Collections.Generic;

namespace BigData_Edu.Models
{
    public class PairData
    {
        public List<Pair> Result { get; set; }
    }

    public class Points
    {
        public List<Point> P0 { get; set; }
        public List<Point> P1 { get; set; }
        public List<Point> P2 { get; set; }
    }

    public class Point
    {
        public double Long { get; set; }
        public double Lat { get; set; }
        public decimal M1 { get; set; }
        public decimal M2 { get; set; }
        public string Number { get; set; }
    }


    public static class PairDataExtensions
    {
        public static PairData Mock(this PairData model)
        {
            model.Result = new List<Pair>
            {
                new Pair
                {
                    RowNumber = 1,
                    Metrica = 1.1,
                    N1 = 10,
                    N2 = 20,
                    Points =new Points
                    {
                        P0 = new List<Point>
                        {
                            new Point{ Lat = 10.1, Long= 11.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                        },
                        P1 = new List<Point>
                        {
                            new Point{ Lat = 10.1, Long= 11.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                        },
                        P2 = new List<Point>
                        {
                            new Point{ Lat = 10.1, Long= 11.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                        },
                    }
                },
                new Pair
                {
                    RowNumber = 1,
                    Metrica = 1.1,
                    N1 = 10,
                    N2 = 20,
                    Points =new Points
                    {
                        P0 = new List<Point>
                        {
                            new Point{ Lat = 10.1, Long= 11.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                        },
                        P1 = new List<Point>
                        {
                            new Point{ Lat = 10.1, Long= 11.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                        },
                        P2 = new List<Point>
                        {
                            new Point{ Lat = 10.1, Long= 11.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                            new Point{ Lat = 20.1, Long= 31.2, Number="1/1" },
                        },
                    }
                },
            };
            return model;
        }
    }
}