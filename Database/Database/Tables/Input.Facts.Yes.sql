﻿CREATE TABLE [dbo].[Input.Facts.Yes]
(
	[msisdn1] bigINT NOT NULL,
	[msisdn2] bigINT NOT NULL, 

    CONSTRAINT [PK_InputData.Facts.Yes] PRIMARY KEY ([msisdn1], [msisdn2]),
)
