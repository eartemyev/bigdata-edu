﻿CREATE TABLE [dbo].[View.Stantions]
(
	[cid] bigint NOT NULL PRIMARY KEY,

	[start_angle] float NOT NULL,
	[end_angle] float NOT NULL,
	[max_dist] float NOT NULL,

	[long] float NOT NULL,
	[lat] float NOT NULL,
)
